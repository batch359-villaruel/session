# Comments
# Comments in python are done using the "#" symbol
# ctrl + / -> one-line comment

"""
#although we have no keybind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""
# Python Syntax
# Hello world in Python
print("Hello world!")

# Indentation
# wherein other programming language
# In Python, indentation is used to indicate a block of code.

# Variables
# The terminology used for variable name is identifier
# All identifiers should begin with a letter(A to Z), dollar sign($) or an underscore
# declaring of variables
age = 20
age = 18
middleInitial = 'C'
print(age)

name1, name2, name3, name4 = "John", "Edwin", "Kenneth", "Rey"
print(name4)

# Data Types
# 1. Strings (str)
full_name = "John Doe"
secret_code = "Pa$$w0rd"

# 2. Numbers (int/float/complex)
num_of_days = 365
pi_approx = 3.1416
complex_num = 1 + 5j

# 3. Booleans (bool) - True and False values only
is_learning = True
is_difficult = False

# Using Variables
print("My Name is " + full_name)

# Terminal outputs
# print() function
print("Hello World Again")
print("My Name is " + full_name)
#print("My age is " + age)
print("My age is " + str(age))

# Typecasting
# There may be times when you want to specify a type on to a variable. This can be done with casting. Here are some functions that can be used:
# 1. int() -> converts the value into an integer value
# 2. float() -> converts the value into a float value
# 3. str() -> converts the value into a strings

print(int(3.5))
print(int("9876"))
print(float(10))

# F-strings
# f-string allows us to embed expressions directly in string literals using curly braces {}
print(f"Hi! My name is {full_name} and my age i {age}")

# Operations
# Addition (+)
print(1 + 10)
# Subtractions
print(15 - 8)
# Multiplication
print(18 * 9)
# Division
print(21 // 7)
# Exponent
print( 2 ** 6)
# Modulo / Remainder
print(18 % 4)

# Assignment Operator
# Addition Operators
num1 = 3
num1 += 4
print(num1)

# Logical Operators
# AND (&)
# NOT (!)
# OR (|)
print(True and False)
print(not True)
print(True or False)

#comparison operators (returns a boolean value)
print(1 != 1)
print(1 == 1)
print(5 > 10)
print(5 < 10)
print(1 <= 10)
print(1 >= 10)

# 4. Lists []
# 5. Tuples ()
# 6. Sets {}
# 7. Dictionaries {}
# 8. NoneType
