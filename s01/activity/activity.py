full_name = 'Janny'
age = 25
occupation = 'IT'
movie = 'Interstellar'
rating = 99.9
print(f"I am {full_name}, and I am {age} years old, I work as a {occupation}, and my rating for movie {movie} is {rating}")

num1, num2, num3 = 10, 20, 30

print(num1 * num2)
print(num1 < num3)
num2 += num3