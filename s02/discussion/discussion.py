# Input
# input() is similar to prompt() in JS that seek to gather data from the user input
# \n stand for line break
# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course!")

# in JS = typeof
# in Python = type()

# num1 = int(input("Enter first number: \n"))
# num2 = input("Enter second number: \n")
# print(f"Your age is {num1 + num2}")
# print(type(num1))

# num1 = int(input("Enter first number: \n"))
# num2 = int(input("Enter second number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# print(type(num1))

# Control  Structures
  # 1. Selection
    # Allow the program to choose among choices and run specific codes depending on the choice taken
  # 2. Repetition
    # Allow the program to repeat certain blocks of code given a starting condition and terminating condition

# If-Else statements
# test_num = 75

# if test_num >= 60:
#   print('Test Passed')
# else:
#   print('Test Failed')

# # The elif is the shorthand for "else if" in other programming language
# test_num2 = int(input("Please enter second test number: \n"))
# if test_num2 > 0:
#   print("The number is positive")
# elif test_num2 == 0:
#   print("The number is zero")
# else:
#   print("The number is negative")

# MINI ACTIVITY
# number = int(input("Please enter a number: \n"))
# if number == 0:
#   print("The number is Zero")
# elif (number % 5 == 0 and number % 3 == 0):
#   print(f"The number {number} is divisible by both 3 and 5")
# elif number % 5 == 0:
#   print(f"The number {number} is divisible by 5")
# elif number % 3 == 0:
#   print(f"The number {number} is divisible by 3")
# else:
#   print(f"The number {number} is NOT divisible by 3 and 5")

# Loops
# Python has loops that can repeat block of codes
# While Loops are used to execute a set of statement as long as the condition is true

# i = 1
# while i <= 5:
#   print(f"The current count {i}")
#   i += 1

# # For Loops are used for iterating over a sequence
# fruits = ["apple", "banana", "cherry"]
# print(fruits)
# for fruit in fruits:
#   print(fruit)

# range() method - returns the sequence of the given number
# Syntax:
# range(stop) - function defaults to 0 as a starting value.
# range(start, stop) - functions start with the first number
# range(start, stop, step) the third argument can be added to specify the increment of the loop
# for x in range(6):
#   print(f"The current value is {x}")

# for x in range(6, 10):
#   print(f"The current value is {x}")

# for x in range(6, 20, 2):
#   print(f"The current value is {x}")

# MINI ACTIVITY #2
# Create a loop to count and display the number of even numbers between 1 and 20.
# count = 0
# for num in range(1, 21):
#   if num % 2 == 0:
#     count += 1

# print(f"The number of even number between 1 and 20 is: {count}")

# Mini Exercise
# Write a Python program that takes an integer input from the user and displays the multiplication table for that number

# num = int(input("Enter a number: "))

# print(f"The multiplication table for {num}:")
# for x in range(1, 11):
#   total = num*x
#   print(f"{num} x {x} = {total}")

# Break Statement - is used to stop the loop
# j = 1
# while j < 6:
#   print(j)
#   if j == 3:
#     break
#   j += 1

# Continue statement - returns the control to the beginning of the while loop and continue with the next iteration
k = 1
while k < 6:
  k += 1
  if k == 3:
    continue
  print(k)