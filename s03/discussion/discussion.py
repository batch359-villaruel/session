# Python has several structure to store or multiple items in a single variable

# Lists - Similar to arrays

names = ["John", "Kenneth", "Luke", "Nueve"] #String list
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20] #Integer list
truth_values = [True, False, False, True, True] #Boolean

random_values = [260, "Hello", True]

print(names)
print(programs)
print(durations)
print(truth_values)
print(random_values)

# Getting the list size
# The number of elements in a list can be counted using the len() method

print(len(programs))

# Accessing values
# Accessing the first values
print(programs[0])

# Accessing the last values
print(programs[-1])

# Accessing the second value
print(programs[1])

# Access a range of values
# List [start index: end index]

print(programs[0:2]) # will display index from 0 upto 1

# Updating lists
print(f"Current value: {programs[-1]}")
programs[-1] = "Short Courses"
print(f"Current value: {programs[2]}")

# Mini Activity
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing in the following format:
# Students: Mary, Matthew, Tom, Anna, Thomas
# Grade: 100, 85, 88, 90, 75
# Sample Output:
# The grade of Mary is: 100
# The grade of Matthew is: 85
# The grade of Tom is: 88
# The grade of Anna is: 90
# The grade of Thomas is: 75

students = ["Mary", "Matthew", "Tom", "Anna", "Thomas"]
grades = [100, 85, 88, 90, 75]

for i in range(len(grades)):
  print(f"The grade of {students[i]} is {grades[i]}")


# List Manipulation
# append() - it will allow us to insert items to list
programs.append('global')
print(programs)

# Delete the last item on the list
del programs[-1]
print(programs)

# Membership checks - the "in" keyword checks if the element is in the list
print(20 in durations)
print(500 in durations)

# Sorting lists - sort() method, by default: ascending
print(students)
students.sort()
print(students)
# Using revers=True: descending
students.sort(reverse=True)
print(students)

# Emptying the List - the clear() method is used to empty the contents of the lists
test_lists = [1, 2, 3, 4, 5]
print(test_lists)
test_lists.clear()
print(test_lists)

# Dictionaries are used to store data values in key: value pairs
person1 = {
  "name": "Brandon",
  "age": 28,
  "occupation": "Developer",
  "isWorking": True,
  "languages": ["Python", "JavaScript", "PHP"]
}
print(person1)
print(len(person1))

# Accessing the values in the dictionary
print("Name:", person1["name"]) # dot notation

# keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# values() method will return a list of all the values in the dictionary
print(person1.values())

# items() method will return each item in a dictionary, as a key-value pair in a list
print(person1.items())

# adding key=value pair
person1['nationality'] = "Filipino"
person1.update({"fave_food": "Sinigang"})

print(person1)

# Deleting entries
del person1["nationality"]
print(person1)

person1.pop("fave_food")
print(person1)

person2 = {
  "name": "John",
  "age": 20
}
print(person2)
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
  print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries
person3 = {
  "name": "Monica",
  "age": 28,
  "occupation": "Software Engineer",
  "isWorking": True,
  "languages": ["Python", "JavaScript", "PHP"]
}

employees = {
  "employee1": person1,
  "employee2": person3
}

print(employees)

# Functions are blocks of code that runs when called
# "def" keyword this is used to create a function. Syntax: def <function_name> (parameter):

# defines a function called my_greeting
def my_greeting():
  # code to be run when my_greeting is called back
  print("Hello User!")
my_greeting()

def greet_user(username):
  print(f"Hello {username}!")
greet_user("Bob")

def greet_user1(username="Bob"):
  print(f"Hello {username}!")
greet_user1()
greet_user1("Kim")

# return statement - "return" keyword allow functions to return values
def addition(num1, num2):
  return num1 + num2
sum = addition(5, 10)
print(sum)

# Lambda Functions
# Lambda function is a small anonymous function that can be used for callback
# lambda parameter : expression
greeting = lambda person : f'Hello {person} '

# def greeting(person):
#   print(f"Hello {person}")

print(greeting("Golteb"))

mult = lambda a, b : a * b
print(mult(5, 6))
25
# Mini Activity
# Create a function that get the square root of a number
number = int(input("Enter a number: "))
sqrt = lambda number : number ** 0.5
print(sqrt(number))

num5 = int(input("Enter a number: "))
def sqrt(num5):
  return num5**0.5
print(sqrt(num5))

# Classes
# This serves as blueprints to describe the concept of objects

# class ClassName():
class Car():
  # Properties
  def __init__(self, brand, model, year_of_make):
    self.brand = brand
    self.model = model
    self.year_of_make = year_of_make
    # other properties can be added and assigned hard-coded values
    self.fuel = "Gasoline"
    self.fuel_level = 0
  
  # method
  def fill_fuel(self):
    print(f"Current fuel level: {self.fuel_level}")
    print("Filling up the fuel tank")
    self.fuel_level = 100
    print(f"New fuel level is: {self.fuel_level}")

new_car = Car("Toyota", "Innova", "2023")
print(f"My Car is a {new_car.brand} {new_car.model}")

new_car.fill_fuel()